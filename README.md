# **Réponses aux questions du TP**

## **Exercice 1 : Premier pipeline de CI/CD**

1. **Quelle est l’image utilisée par la CI ?**  
   - L'image utilisée est celle par défaut de GitLab, mais elle doit être spécifiée explicitement. 
   - **Correction** : Ajouter cette ligne dans `.gitlab-ci.yml` :
     ```yaml
     image: python:alpine3.16
     ```

2. **Quel est le nom du runner sur lequel le job est effectué ?**  
   - Visible dans **CI/CD > Pipelines > Jobs** sur GitLab.
   - GitLab utilise des **shared runners** ou un **runner spécifique** si configuré.

3. **Pourquoi la commande `python --version` ne marche-t-elle pas au début ?**  
   - L’image par défaut de GitLab CI/CD **ne contient pas Python**.
   - **Solution** : Utiliser une image contenant Python :
     ```yaml
     image: python:alpine3.16
     ```

4. **Faites en sorte de changer l’image utilisée et retestez.**  
   - Après modification, la commande `python --version` fonctionnera.

---

## **Exercice 2 : Mise en place d’un projet avec tests unitaires**

1. **Qu’est-ce que vous constatez au lancement des tests unitaires ?**  
   - Les tests échouent car **GitLab ne trouve pas `project.py`**.

2. **Trouvez la raison et une solution pour exécuter la CI correctement.**  
   - **Problème** : GitLab exécute les tests sans connaître les dépendances du projet.
   - **Solution** :
     ```yaml
     test-job: 
       stage: test
       image: python:alpine3.16
       script:
         - pip install -r requirements.txt || true
         - python3 test.py
     ```

---

## **Exercice 3 : Ajout du Lint (qualité du code)**

1. **Que constatez-vous lors du lancement de la CI ? Pourquoi ?**  
   - `pylint` peut **émettre des erreurs** et empêcher le pipeline de passer.

2. **Comment régler ça ?**  
   - **Ajout d’un fichier `.pylintrc`** pour ajuster les règles de `pylint`.
   - **Corriger les erreurs dans `project.py`**.

3. **Quel est le problème avec l’installation d’un package dans une CI ?**  
   - L’installation des dépendances **ralentit l’exécution** et peut causer des erreurs.

4. **Comment résoudre ce problème ?**  
   - **Utiliser un cache** pour stocker les paquets :
     ```yaml
     cache:
       paths:
         - ~/.cache/pip
     ```
   - **Utiliser une image Docker préconfigurée** avec `pylint` déjà installé.

---

## **Exercice 4 : Gestion des artéfacts (fichiers générés dans la CI)**

1. **Comment récupérer l’artéfact ?**  
   - Disponible dans **CI/CD > Jobs > lint-job > Download Artifacts**.

2. **Combien de temps reste-t-il disponible sur GitLab ?**  
   - Par défaut **30 jours**, mais configurable avec :
     ```yaml
     artifacts:
       expire_in: 1h
     ```

---

## **Exercice 5 : Passer un artéfact d’un job à l’autre**

1. **Que constatez-vous après l’exécution de la CI ?**  
   - `echo-lint-job` échoue car il **ne trouve pas l’artéfact**.

2. **Quelle commande permet de vérifier si le fichier existe ?**  
   - Dans `echo-lint-job` :
     ```sh
     ls -la output  # Vérifie le dossier
     test -f output/result.txt && echo "Fichier trouvé !" || echo "Fichier non trouvé."
     ```

3. **Solution en utilisant la documentation GitLab.**  
   - **Correction dans `.gitlab-ci.yml`** :
     ```yaml
     lint-job: 
       stage: test
       image: python:alpine3.16
       script:
         - apk update && apk add py3-pip
         - pip install pylint
         - mkdir -p output
         - pylint project.py > output/result.txt || touch output/result.txt
       artifacts:
         paths:
           - output/result.txt
         expire_in: 1h
     
     echo-lint-job:
       stage: test
       script:
         - ls -la output
         - test -f output/result.txt && echo "Fichier trouvé !" || echo "Fichier non trouvé."
         - cat output/result.txt
       needs:
         - job: lint-job
           artifacts: true
     ```

